# Project Stria Blueprint

Named after the neural tissue of the brain,
stria terminalis, whose activity correlates with anxiety
in response to threat monitoring.

Quite a fitting name for an alerting system. 

# Goals

The Stria project should accomplish the following:
 - create a alerting system backed by RFCs and available to
 everyone for no charge
 - Define a set of RFCs defining:
	- a JSON format alerting API endpoint structure compatible with REST, gRPC,
	SMTP
	- a XML interface for legacy support (if needed)
	- a standard JSON output format for push notifications to mobile devices
	- a standard JSON output format for push notifications to other devices
	- a pacemaker protocol for checking server status
 - adhere to RFCs and open formats wherever possible
 - the project should create the following integrations with third-party services:
	- SMS gateways using SMPP over IPSec tunnels
	- Call APIs
 - the project should natively integrate with Kubernetes
 - the project should natively integrate with Prometheus
 - the project should natively integrate with log analysis tools (syslog formatted,
 ELK stack)
 - the project should offer an Android, iPhone application for receiving alerts
 - the mobile applications have to free of charge
 - allow for IoT integrations to Stria
